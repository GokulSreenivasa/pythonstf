'''
Created on 11-May-2018

@author: gokul
'''
import configparser

import os.path


config = configparser.ConfigParser()
config.sections()

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
requiredPath=os.path.abspath(os.path.join(ROOT_DIR, os.pardir))

config.read(requiredPath+'/ConfigFile.properties')

def getValue(v):
    value=config.get('Properties',v)
    print("The read value for the property " , v , "is " , value)
    return value