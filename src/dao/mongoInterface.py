from pymongo import MongoClient
from readProperty import getValue


client = MongoClient(getValue("mongoIp"),27017) 
db = client.EmployeeData
print ('\n Connection Stablished : {}'.format(db))
# def __init__():
#     client = MongoClient('103.67.236.96',27017) 
#     db = client.EmployeeData
#     print ('\n Connection Stablished : \n {}'.format(db))

# collation=Collation(locale='en_US')

def getCollection():
    if not db.Inventory:
        return db.create_collection("Inventory")
    else :
        return db.Inventory
    print(db.Inventory)    

# print("Collection : {}".format(getCollection()))

def addToCollection(data):
    db.Inventory.insert_one(data)


def getAllData():
    return db.Inventory.find()

# filter is a json object or Array of required data filter.

def getFilteredData(filter):
    return db.Inventory.find(filter)

"""
targetKeyJSON = {"name":"John"}
updateValueJSON = {"name":"Joseph"}
"""
def updateSingleDocument(key, value):
    # logic : fetch document with feild. then Change that feild into json , and update.
    return db.Inventory.update(key, {"$set":value})

def updateAllDocument(key, value):
    # logic : fetch document with feild. then Change that feild into json , and update.
    return db.Inventory.update_many(key, {"$set":value})

def deleteAllDocument(filter):
    return db.Inventory.deleteMany(filter)