'''
Created on 07-May-2018

@author: gokul
'''
#from ipfsDataHandler import postFile
from swarmData import repost_file
from pymongo import MongoClient
from idGenerator import NewCustomerID
from web3 import HTTPProvider , Web3
from datetime import datetime
from readProperty import getValue

def upload_file(request):
    client = MongoClient(getValue('mongoIp'), 27017)
    db = client.TradeFinance    
    # Ipfs Data storage
    result={}
    documentData = []
    print("The Country is ",request.form.get("country"))
    cId = request.form.get('customerId', None)
    print("The cid is " , cId)
    if cId is None:
        print("Inside Create Profile")
        customerId = NewCustomerID(request.form.get("country"))
        
        data = {"customerId":customerId}
        for key in request.files:
            print (key, 'corresponds to', request.files[key])
            fileswarm = request.files[key]
            response = repost_file(fileswarm)
            print("the response from swarm is ", response , "for the file" , fileswarm)  
            doc={}
            doc[key]= request.files[key]
            doc['hash']=response
            documentData.append(doc)
        data.update({"documents":documentData,"addedDate":datetime.now() , "modifiedDate":datetime.now()})
        print("documentData is ", documentData)        
        
        db.IpfsDocuments.insert_one(data) 
        print("The inserted data  is " , data)
    
        # Info storage in monogodb
        info = {"customerId":customerId,"addedDate":datetime.now() , "modifiedDate":datetime.now()}   
        for key in request.form:
            print (key, 'corresponds to', request.form.get(key))      
            info[key] = request.form.get(key)
        print("The fields are ", info)      
        
        db.KycInfo.insert_one(info) 
        
        w3 = Web3(HTTPProvider(getValue('ethereumUrl')))
        print("connection successful", w3)
        address = w3.personal.newAccount(request.form.get("secretKey")) 
        
        db.ethereumData.insert_one({"customerId":customerId , "password":request.form.get("secretKey") , "address":address  ,"addedDate":datetime.now() , "modifiedDate":datetime.now() })
        result['address']=address
        result['customerId']=customerId
    else :
        #data = {"customerId":customerId}
        for key in request.files:
            print (key, 'corresponds to', request.files[key])
            fileswarm = request.files[key]
            response = repost_file(fileswarm)
            print("the response from swarm is ", response , "for the file" , fileswarm)  
            db.IpfsDocuments.update ({"customerId": cId,"documents.file":fileswarm}, { '$set': {"documents.$.hash": response, "modifiedDate":datetime.now()} });
            
        for key in request.form:
            print (key, 'corresponds to', request.form.get(key))  
            updatedvalue=request.form.get(key)    
            db.KycInfo.update ({"customerId": cId}, { '$set': {key: updatedvalue, "modifiedDate":datetime.now()} });
        
        
    return result
  
