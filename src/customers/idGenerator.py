'''
@author = Kishore Varma Alluri
Purpose: Will return the next sequence of a Customer ID based on his Country Code that is a mandatory parameter

'''


#from singleton import getDbConnection	
from pymongo import MongoClient	
from readProperty import getValue

def NewCustomerID(country):
	client = MongoClient('mongodb://'+getValue('mongoIp')+':27017')	
    #client = MongoClient('10.10.10.49',27017)
	#client= getDbConnection()
	response = {}
	if client is None:
		response["nid"] = ''
		response["status"] = 'F'
		response["remarks"] = 'Connection to database failed'
		return response

	try:
		#connect to the db TradeFinance
		dbname = client.TradeFinance		
		#Connect to the required collection
		countryCode = dbname['countryCode']		
		#Fetch the record with the country
		rec = countryCode.find_one({"Country":country.upper()})		
		#get the id number counter
		rec['counter'] = rec['counter']+1		
		#create the customer id as country code + 7 digits of the counter
		response["nid"] = rec['Code']+format(rec['counter'],'07')		
		#update the counter in the table
		countryCode.update({'_id':rec['_id']},rec)
		
		response["status"] = 'S'
		response["remarks"] = ' '
		print("the customer id is " , response['nid'])
		
	except:
		response["status"] = 'F'
		response["remarks"] = 'Something Failed'
		
	return response['nid']



	
