'''
Created on 07-May-2018

@author: gokul
'''
from flask import Flask,request ,jsonify
from kycData import upload_file
from mongoInterface import getAllData,getFilteredData ,updateAllDocument ,deleteAllDocument

app = Flask(__name__)


@app.route('/', methods = ['GET'])
def intro():
    return "Welcome to Backend of STF"      
    
@app.route('/TradeFinance/kyc', methods=['POST'])
def kyc():
    response= upload_file(request)
   
    return "{}".format(response)


@app.route('/getCollection',methods=['POST'])
def allCollections():
    reqJSON = request.data
    print(reqJSON)
    cursor = getAllData()
    coll = []
    for c in cursor:
        coll.append(c)        
    # print(coll)
    return "{}".format(coll)

@app.route('/filterCollection',methods=['POST'])
def filterCollection():
    reqJSON = request.get_json()
    print(reqJSON)
    cursor = getFilteredData(reqJSON)
    coll = []
    if cursor is not None :
        for c in cursor:
            coll.append(c)        
        print(coll)
    return jsonify("{}".format(coll))



@app.route('/update',methods=['POST'])
def manipulateDocument():
    reqJSON = request.get_json()
    targetJSON = reqJSON['filter']
    updatedValueJSON = reqJSON['data']
    print("".format(targetJSON))
    print("".format(updatedValueJSON))
    resUpdate=updateAllDocument(targetJSON,updatedValueJSON)
    #coll = []
    # if cursor is not None :
    #     for c in cursor:
    #         coll.append(c)        
    #     print(coll)
    return jsonify("{}".format(resUpdate))

@app.route('/deleteSelectedAll',methods=['POST'])
def removeSelected():
    reqJSON = request.get_json()
    resUpdate=deleteAllDocument(reqJSON)
    return jsonify("{}".format(resUpdate))
   
    
if __name__ == '__main__':
    app.run(debug=True)
