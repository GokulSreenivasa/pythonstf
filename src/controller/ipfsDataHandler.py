'''
Created on 07-May-2018

@author: gokul
'''
import ipfsapi
from readProperty  import getValue
#from astropy.io.fits.convenience import getval


def postFile(file):
    # connect to server node
    try:
        ip=getValue('ipfsIp')
        port=getValue('ipfsPort')
        api = ipfsapi.connect(ip, port)
        
        # print(api)
    
    except ipfsapi.exceptions.ConnectionError as ce:
        
        print(str(ce))
    
    response = api.add(file)    
    print("the response is ", response)
    return response
