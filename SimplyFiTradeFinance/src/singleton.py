'''
Created on 07-May-2018

@author: gokul
'''

from pymongo import MongoClient
class Singleton:
    # Here will be the instance stored.
    __instance = None

@staticmethod
def getDbConnection():
    """ Static access method. """
    if Singleton.__instance == None:
        client = MongoClient('103.67.236.96',27017) 
        db = client.TradeFinance
    return db          
    
def __init__(self):
    """ Virtually private constructor. """
    if Singleton.__instance != None:
        raise Exception("This class is a singleton!")
    else:
        Singleton.__instance = self