'''
Created on 07-May-2018

@author: gokul
'''
from ipfsDataHandler import postFile
from pymongo import MongoClient
from idGenerator import NewCustomerID
from web3 import HTTPProvider , Web3
    
def upload_file(request):
    
    # Ipfs Data storage
    documentData = []
    customerId = NewCustomerID(request.form.get("country"))
    data = {"customerId":customerId}
    for key in request.files:
        print (key, 'corresponds to', request.files[key])
        fileIpfs = request.files[key]
        response = postFile(fileIpfs)  
        documentData.append(response)
    data.update({"documents":documentData})
    print("documentData is ", documentData)
     
    client = MongoClient('103.67.236.96', 27017) 
    db = client.TradeFinance
    db.IpfsDocuments.insert_one(data)  
    print("The inserted data  is " , data)

    # Info storage in monogodb
    info = {"customerId":customerId}
   
    for key in request.form:
        print (key, 'corresponds to', request.form.get(key))      
        info[key] = request.form.get(key)
    print("The fields are ", info)      
    
    db.KycInfo.insert_one(info) 
    
    w3 = Web3(HTTPProvider('http://10.10.10.66:8546'))
    print("connection successful", w3)
    address = w3.personal.newAccount(request.form.get("secretKey")) 
    
    db.ethereumData.insert_one({"customerId":customerId , "password":request.form.get("secretKey") , "address":address })
    return 'file uploaded successfully'
  
