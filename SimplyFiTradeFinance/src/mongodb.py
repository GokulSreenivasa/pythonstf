'''
Created on 07-May-2018

@author: gokul
'''


import sys
sys.path.insert(0,'src')
# sys.path.insert(0,'/src/controller')
sys.path.insert(0,'src/customers')
sys.path.insert(0,'src/dao')
sys.path.insert(0,'src/interface')

from pymongo import MongoClient

@staticmethod
def dbConnection():
    client = MongoClient('103.67.236.96',27017) 
    db = client.TradeFinance
    return db
    
