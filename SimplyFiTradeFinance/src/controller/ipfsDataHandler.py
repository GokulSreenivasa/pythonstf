'''
Created on 07-May-2018

@author: gokul
'''
import ipfsapi

def postFile(file):
    # connect to server node
    try:
        api = ipfsapi.connect('182.156.236.59', 5001)
    
        # print(api)
    
    except ipfsapi.exceptions.ConnectionError as ce:
        
        print(str(ce))
    
    response = api.add(file)    
    print("the response is ", response)
    return response
